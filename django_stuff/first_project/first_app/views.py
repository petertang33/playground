from django.shortcuts import render
from django.http import HttpResponse

# Create your views here.

def index(request):
    my_dict = {
    'insert_me':"1Hello I am from views.py",
    'insert_me2':"2Hello I am from views.py",
    }
    return render(request,'first_app/index.html',context = my_dict)
